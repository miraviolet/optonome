<?php
/**
 * The template used for displaying a Audience Options block.
 *
 * @package _s
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$text            = get_sub_field( 'text' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );
$audience     = get_sub_field( 'audience' );
$animation_class = _s_get_animation_class();

// Start a <container> with possible block options.
_s_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block audience-options grid-container', // Container class.
	)
);
?>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
		<div class="cell">
			<?php if ( $title ) : ?>
				<h3 class="audience-options-title"><?php echo esc_html( $title ); ?></h3>
			<?php endif; ?>

			<?php if ( $text ) : ?>
				<h4 class="audience-options-text"><?php echo esc_html( $text ); ?></h4>
			<?php endif; ?>
        </div>
        
        <div class="cell">
            <?php if( have_rows('audience') ): ?>
            <?php while( have_rows('audience') ): the_row();  
            
            $link     = get_sub_field( 'link' );
            $name     = get_sub_field( 'name' );

            ?>

			<a class="audience-option" href="<?php echo esc_html( $link ); ?>">
				<?php echo esc_html( $name ); ?>
			</a> 
            <?php endwhile; ?>

            <?php endif; ?>
        </div>
	</div><!-- .grid-x -->
</section><!-- .cta-block -->
