<?php
/**
 * The template used for displaying a Services block.
 *
 * @package _s
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$text            = get_sub_field( 'text' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );
$services     = get_sub_field( 'services' );
$animation_class = _s_get_animation_class();
$link_text     = get_sub_field( 'link_text' );
$link     = get_sub_field( 'link' );

// Start a <container> with possible block options.
_s_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container services', // Container class.
	)
);
?>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
		<div class="cell">
			<?php if ( $title ) : ?>
				<h3 class="services-title"><?php echo esc_html( $title ); ?></h3>
			<?php endif; ?>

			<?php if ( $text ) : ?>
				<h4 class="services-text"><?php echo esc_html( $text ); ?></h4>
			<?php endif; ?>
		</div>

		<div class="cell services-wrap">
			<?php if( have_rows('service') ): ?>
			<?php while( have_rows('service') ): the_row();  
			
			$icon     = get_sub_field( 'icon' );
			$name     = get_sub_field( 'name' );
			$blurb     = get_sub_field( 'blurb' );
			
			?>

			<div class="service">
				<img src="<?php echo esc_html( $icon ); ?>" class="service-icon"> 
				<p class="service-name"><?php echo esc_html( $name ); ?></p>
				<p class="service-blurb"><?php echo esc_html( $blurb ); ?></p>
			</div>

			<?php endwhile; ?>

			<?php endif; ?>
		</div>

		<div class="cell">
			<a class="services-link" href="<?php echo esc_html( $link ); ?>"><?php echo esc_html( $link_text ); ?></a>
		</div>
	</div><!-- .grid-x -->
</section><!-- .cta-block -->
